package ipeer.mentor.task;

import android.content.*;
import android.widget.*;
import entity.LocalVeteran;
import entity.Veteran;
import ipeer.util.AndroidUtils;
import ipeer.util.Client;
import roboguice.util.RoboAsyncTask;

public class GenerateResetTokenTask extends RoboAsyncTask<Void> {
  private Client client;
  private LocalVeteran localVeteran;

  public GenerateResetTokenTask(final Context context, final Client client, final LocalVeteran localVeteran) {
    super(context);
    this.client = client;
    this.localVeteran = localVeteran;
  }

  @Override
  public Void call() throws Exception {
    final Veteran veteran = new Veteran();
    veteran.setId(localVeteran.getId());
    client.resetPassword(veteran);

    return null;
  }

  @Override
  protected void onSuccess(final Void nothing) throws Exception {
    final String message = String.format("Username: %s\n" +
        "Login here: %s\n" +
        "Ask your mentor for the password", localVeteran.getId(), "https://dryhootch-qrf.com");
    AndroidUtils.sendSMS(localVeteran.getPhone(), message, getContext());
    Toast.makeText(getContext(), "Password reset information is sent to: " + localVeteran.getPhone(), Toast.LENGTH_LONG).show();
  }

  @Override
  protected void onException(final Exception e) throws RuntimeException {
    super.onException(e);
    Toast.makeText(getContext(), "Failed to reset password for the veteran", Toast.LENGTH_LONG).show();
  }
}
