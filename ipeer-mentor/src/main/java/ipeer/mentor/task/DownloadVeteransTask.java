package ipeer.mentor.task;

import android.app.*;
import android.content.*;
import android.util.*;
import android.widget.*;
import entity.Veteran;
import ipeer.util.Application;
import ipeer.util.Client;
import roboguice.util.RoboAsyncTask;

import java.util.List;

public class DownloadVeteransTask extends RoboAsyncTask<List<Veteran>> {
  private final ProgressDialog dialog;
  private final Application application;
  private final Client client;
  private final Callback callback;

  public DownloadVeteransTask(final Context context, final Application application, final Client client, final Callback callback) {
    super(context);
    this.application = application;
    this.client = client;
    this.callback = callback;
    this.dialog = new ProgressDialog(context);
  }

  @Override
  protected void onPreExecute() {
    dialog.setMessage("Downloading veteran information");
    dialog.show();
  }

  @Override
  public List<Veteran> call() throws Exception {
    return client.getVeterans();
  }

  @Override
  protected void onSuccess(final List<Veteran> veterans) throws Exception {
    application.setVeterans(veterans);
    callback.call();
  }

  @Override
  protected void onException(final Exception e) throws RuntimeException {
    super.onException(e);
    Log.d(getClass().getName(), e.getMessage(), e);
    Toast.makeText(getContext(), "Failed to download veterans" + e.getMessage(), Toast.LENGTH_LONG).show();
  }

  protected void onFinally() {
    if (dialog.isShowing()) {
      dialog.dismiss();
    }
  }

  public interface Callback {
    void call();
  }

}
