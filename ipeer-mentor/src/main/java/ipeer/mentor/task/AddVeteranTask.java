package ipeer.mentor.task;

import android.content.*;
import android.widget.*;
import entity.LocalVeteran;
import entity.Veteran;
import ipeer.util.Application;
import ipeer.util.Client;
import ipeer.util.LocalDatabaseClient;
import roboguice.util.RoboAsyncTask;

public class AddVeteranTask extends RoboAsyncTask<Veteran> {

  private final LocalVeteran localVeteran;
  private final Application application;
  private final Client client;
  private final LocalDatabaseClient localClient;
  private final Callback callback;

  public AddVeteranTask(final Context context, final Application application, final Client client, final LocalDatabaseClient localClient, final LocalVeteran localVeteran, final Callback callback) {
    super(context);
    this.localVeteran = localVeteran;
    this.application = application;
    this.client = client;
    this.localClient = localClient;
    this.callback = callback;
  }

  @Override
  public Veteran call() throws Exception {
    Veteran veteran = new Veteran();
    veteran.setMentor(0);
    return client.createVeteran(veteran);
  }

  @Override
  protected void onSuccess(final Veteran veteran) throws Exception {
    Toast.makeText(getContext(), "New veteran added", Toast.LENGTH_LONG).show();
    localVeteran.setId(veteran.getId());
    localClient.addVeteran(localVeteran);
    new GenerateResetTokenTask(getContext(), client, localVeteran).execute();

    new DownloadVeteransTask(getContext(), application, client, new DownloadVeteransTask.Callback() {
      @Override
      public void call() {
        callback.call(localVeteran);
      }
    }).execute();
  }

  @Override
  protected void onException(final Exception e) throws RuntimeException {
    super.onException(e);
    Toast.makeText(getContext(), "Failed to add veteran: " + e.getMessage(), Toast.LENGTH_LONG).show();
  }

  public interface Callback {
    void call(final LocalVeteran veteran);
  }
}
