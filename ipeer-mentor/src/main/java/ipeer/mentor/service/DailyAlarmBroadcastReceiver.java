package ipeer.mentor.service;

import android.app.*;
import android.content.*;
import android.net.*;
import android.util.*;
import ipeer.mentor.R;
import ipeer.util.*;
import ipeer.util.Config;
import roboguice.util.RoboAsyncTask;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.Calendar;

public class DailyAlarmBroadcastReceiver extends BroadcastReceiver {
  public static final String APK = "mentor";

  private final Client client;

  public DailyAlarmBroadcastReceiver() {
    client = new Client();
  }

  @Override
  public void onReceive(final Context context, final Intent intent) {
    downloadUpdateAndNotify(context);
  }

  private void downloadUpdateAndNotify(final Context context) {
    new RoboAsyncTask<File>(context) {
      @Override
      public File call() throws Exception {
        File file = null;
        try {
          final URI uri = client.checkForUpdate();
          if(uri != null) {
            Log.d(getClass().getName(), "New update available.");
            file = client.downloadFile(uri, APK);
          }
        }
        catch (IOException e) {
          Log.d(getClass().getName(), "Error downloading latest update");
        }
        return file;
      }

      @Override
      protected void onSuccess(final File file) throws Exception {
        super.onSuccess(file);
        if(file != null) {
          showInstallNotification(context, file);
        }
      }
    }.execute();
  }

  private void showInstallNotification(final Context context, final File apk) {
    final Intent intent = promptInstall(apk);
    final PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
    final Notification notification = new Notification.Builder(context)
        .setSmallIcon(R.drawable.ic_launcher)
        .setContentTitle("New update downloaded")
        .setContentText(apk.getName() + " is ready to be installed")
        .setAutoCancel(false)
        .setContentIntent(pendingIntent)
        .getNotification();
    NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    notificationManager.notify(0, notification);
  }

  private Intent promptInstall(File outputPath) {
    return new Intent(Intent.ACTION_VIEW)
        .setDataAndType(Uri.fromFile(outputPath), "application/vnd.android.package-archive")
        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
  }

  public static void registerDailyAlarmListener(final Context context) {
    Intent downloadAlarmReceiverIntent = new Intent(context, DailyAlarmBroadcastReceiver.class);
    final PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, downloadAlarmReceiverIntent, PendingIntent.FLAG_UPDATE_CURRENT);

    long triggerTimeInMillis = getTriggerTimeInMillis();
    AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
    alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, triggerTimeInMillis, Config.getInstance().getUpdateInterval(), pendingIntent);
  }

  private static long getTriggerTimeInMillis() {
    Calendar calendar = Calendar.getInstance();
    calendar.setTimeInMillis(System.currentTimeMillis());
    calendar.set(Calendar.HOUR_OF_DAY, 0);
    calendar.set(Calendar.MINUTE, 1);
    return calendar.getTimeInMillis();
  }

}
