package ipeer.mentor.component;

import android.view.*;
import android.widget.*;
import entity.LocalVeteran;
import entity.Veteran;
import ipeer.mentor.R;
import ipeer.mentor.activities.VeteranListActivity;
import ipeer.util.LocalDatabaseClient;

public class VeteranArrayAdapter extends ArrayAdapter<Veteran> {

  private VeteranListActivity veteranListActivity;
  private LocalDatabaseClient localClient;

  public VeteranArrayAdapter(final VeteranListActivity veteranListActivity, final LocalDatabaseClient localClient) {
    super(veteranListActivity, R.layout.veteran_summary_layout);
    this.veteranListActivity = veteranListActivity;
    this.localClient = localClient;
  }

  @Override
  public View getView(final int position, final View convertView, final ViewGroup parent) {
    final Veteran veteran = getItem(position);
    LocalVeteran localVeteran = localClient.getVeteran(veteran.getId());
    final LocalVeteran localVeteran1 = localVeteran == null ? new LocalVeteran(veteran.getId()) : localVeteran;
    VeteranSummaryView veteranSummaryView = new VeteranSummaryView(veteranListActivity, parent, localVeteran1, veteran.getSurveyResults());
    VeteranSummaryView.init(veteranSummaryView);
    return veteranSummaryView.getRootView();
  }
}
