package ipeer.mentor.component;

import android.content.*;
import android.view.*;
import android.widget.*;
import com.androidplot.xy.*;
import entity.LocalVeteran;
import entity.SurveyResult;
import ipeer.mentor.R;
import ipeer.mentor.activities.VeteranDetailActivity;
import ipeer.mentor.activities.VeteranEditActivity;
import ipeer.mentor.activities.VeteranListActivity;
import ipeer.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

public class VeteranSummaryView {
  private final LocalVeteran localVeteran;
  private final List<SurveyResult> surveyResults;

  private final View view;
  private TextView textViewName;
  private ImageView thumbOne;
  private ImageView thumbTwo;
  private VeteranListActivity context;

  public VeteranSummaryView(final VeteranListActivity context, final ViewGroup parent, final LocalVeteran localVeteran, final List<SurveyResult> surveyResults) {
    LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    view = inflater.inflate(R.layout.veteran_summary_layout, parent, false);
    this.surveyResults = CollectionUtils.isEmpty(surveyResults) ? Collections.<SurveyResult>emptyList() : surveyResults;
    this.localVeteran = localVeteran;
    this.context = context;
  }

  public void initModels() {
  }

  public void initComponents() {
    textViewName = (TextView) view.findViewById(R.id.veteranName);
    thumbOne = (ImageView) view.findViewById(R.id.thumbOne);
    thumbTwo = (ImageView) view.findViewById(R.id.thumbTwo);
  }

  public void initListeners() {
    ImageView face = (ImageView) view.findViewById(R.id.image);
    face.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(final View v) {
        Intent intent = new Intent(context, VeteranEditActivity.class);
        intent.putExtra(VeteranEditActivity.VETERAN_ID, localVeteran.getId());
        context.startActivityForResult(intent, 0);
      }
    });
  }

  public void initLayout() {
    textViewName.setText(localVeteran.getName());

    thumbOne.setBackgroundResource(getSurveyResultIcon(Calendar.MONDAY));
    thumbTwo.setBackgroundResource(getSurveyResultIcon(Calendar.FRIDAY));
    showGraph();
  }

  private int getSurveyResultIcon(final int surveyDay) {
    final Calendar instance = Calendar.getInstance();
    if(surveyResults.size() > 0) {
      final SurveyResult surveyResult = surveyResults.get(0);
      instance.setTime(surveyResult.getStartTime());
      if(instance.get(Calendar.DAY_OF_WEEK) == surveyDay) {
        return surveyResult.isImproved() == null ? R.drawable.not :
            surveyResult.isImproved() ? R.drawable.thumbsup : R.drawable.thumbsdown;
      }
    }

    if(surveyResults.size() > 1) {
      final SurveyResult surveyResult = surveyResults.get(1);
      instance.setTime(surveyResult.getStartTime());
      if(instance.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY) {
        return surveyResult.isImproved() == null ? R.drawable.not :
            surveyResult.isImproved() ? R.drawable.thumbsup : R.drawable.thumbsdown;
      }
    }

    return R.drawable.not;
  }

  private void showGraph() {
    final XYPlot plot = (XYPlot) view.findViewById(R.id.plot);
    plot.setRangeBoundaries(0, 3, BoundaryMode.FIXED);
    plot.setRangeStepValue(4);
    plot.getLayoutManager().remove(plot.getLegendWidget());
    plot.setDomainStep(XYStepMode.SUBDIVIDE, 1);

    List<Float> scores = new ArrayList<Float>();
    final int size = surveyResults.size();
    for (int i = 0; i < 4; i++) {
      scores.add(i < surveyResults.size() ? surveyResults.get(i).getAverageScore() : 0);
    }
    LineAndPointFormatter formatter = new LineAndPointFormatter();
    formatter.configure(context.getApplicationContext(), R.xml.line_point_formatter);

    XYSeries series = new SimpleXYSeries(scores, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, null);
    plot.addSeries(series, formatter);

    plot.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(final View v) {
        Intent intent = new Intent(context, VeteranDetailActivity.class);
        intent.putExtra(VeteranDetailActivity.VETERAN_ID, localVeteran.getId());
        context.startActivity(intent);
      }
    });
  }

  public View getRootView() {
    return view;
  }

  public static void init(final VeteranSummaryView veteranSummaryView) {
    veteranSummaryView.initModels();
    veteranSummaryView.initComponents();
    veteranSummaryView.initListeners();
    veteranSummaryView.initLayout();
  }
}
