package ipeer.mentor.activities;

import android.os.*;
import android.view.*;
import android.widget.*;
import com.google.inject.Inject;
import entity.LocalVeteran;
import ipeer.mentor.R;
import ipeer.mentor.task.GenerateResetTokenTask;
import ipeer.util.Client;
import ipeer.util.LocalDatabaseClient;
import roboguice.activity.RoboActivity;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

@ContentView(R.layout.veteran_edit_layout)
public class VeteranEditActivity extends RoboActivity {
  public static final String VETERAN_ID = "veteranId";

  @Inject
  private Client client;
  private LocalDatabaseClient localClient = new LocalDatabaseClient(this);

  @InjectView(R.id.name_text_view)
  private TextView nameTextView;
  @InjectView(R.id.phone_text_view)
  private TextView phoneTextView;
  @InjectView(R.id.email_text_view)
  private TextView emailTextView;
  @InjectView(R.id.save_button)
  private Button saveButton;
  @InjectView(R.id.cancel_button)
  private Button cancelButton;
  @InjectView(R.id.change_password)
  private Button changePassword;

  private LocalVeteran localVeteran;

  @Override
  protected void onCreate(final Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    localVeteran = getLocalVeteran();
    initState();
    initListeners();
  }

  private void initState() {
    nameTextView.setText(localVeteran.getName());
    phoneTextView.setText(localVeteran.getPhone());
    emailTextView.setText(localVeteran.getEmail());
  }

  private void initListeners() {
    saveButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(final View v) {
        localVeteran.setName(nameTextView.getText().toString());
        localVeteran.setPhone(phoneTextView.getText().toString());
        localVeteran.setEmail(emailTextView.getText().toString());
        localClient.updateVeteran(localVeteran);

        Toast.makeText(VeteranEditActivity.this, "Veteran information is updated", Toast.LENGTH_SHORT).show();
        finish();
      }
    });

    cancelButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(final View v) {
        finish();
      }
    });

    changePassword.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(final View v) {
        new GenerateResetTokenTask(VeteranEditActivity.this, client, localVeteran).execute();
      }
    });
  }

  private LocalVeteran getLocalVeteran() {
    int veteranId = getIntent().getIntExtra(VETERAN_ID, 0);
    LocalVeteran localVeteran = localClient.getVeteran(veteranId);
    if(localVeteran == null) {
      localVeteran = new LocalVeteran(veteranId);
    }
    return localVeteran;
  }
}
