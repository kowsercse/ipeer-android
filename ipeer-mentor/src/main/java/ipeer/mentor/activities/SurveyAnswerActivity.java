package ipeer.mentor.activities;

import android.app.*;
import android.content.*;
import android.os.*;
import android.util.*;
import android.view.*;
import android.widget.*;
import com.google.inject.Inject;
import entity.Rotation;
import entity.Survey;
import entity.Veteran;
import entity.VeteranQuestionAnswer;
import ipeer.mentor.R;
import ipeer.util.Client;
import roboguice.activity.RoboActivity;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;
import roboguice.util.RoboAsyncTask;

import java.util.List;

@ContentView(R.layout.survey_answer_layout)
public class SurveyAnswerActivity extends RoboActivity {

  public static final String NO_ANSWER = "No Answer";

  @InjectView(R.id.survey_title)
  private TextView textViewSurveyTitle;
  @InjectView(R.id.survey_answers)
  private ListView listViewSurveyAnswers;

  @Inject
  private Client client;

  @Override
  protected void onCreate(final Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    Veteran veteran = loadVeteran();
    final Rotation rotation = loadRotation();
    downloadRotationAnswers(veteran, rotation);

    textViewSurveyTitle.setText(rotation.getSurvey().getTitle());
  }

  private void downloadRotationAnswers(final Veteran veteran, final Rotation rotation) {
    new RoboAsyncTask<List<VeteranQuestionAnswer>>(this) {
      protected ProgressDialog dialog = new ProgressDialog(SurveyAnswerActivity.this);

      @Override
      protected void onPreExecute() {
        dialog.setMessage("Downloading answers");
        dialog.show();
      }

      @Override
      public List<VeteranQuestionAnswer> call() throws Exception {
        return client.getRotationAnswers(veteran, rotation);
      }

      @Override
      protected void onSuccess(final List<VeteranQuestionAnswer> veteranQuestionAnswers) throws Exception {
        ArrayAdapter<VeteranQuestionAnswer> adapter = new ArrayAdapter<VeteranQuestionAnswer>(
            getContext(), R.layout.answer_row_layout, veteranQuestionAnswers) {
          @Override
          public View getView(final int position, final View convertView, final ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.answer_row_layout, parent, false);
            TextView textViewQuestion = (TextView) rowView.findViewById(R.id.question_title);
            TextView textViewAnswer = (TextView) rowView.findViewById(R.id.question_answer);

            VeteranQuestionAnswer answer = getItem(position);
            textViewQuestion.setText(answer.getTitle());
            textViewAnswer.setText("> " + retrieveAnswerText(answer));

            return rowView;
          }
        };

        listViewSurveyAnswers.setAdapter(adapter);
      }

      @Override
      protected void onException(final Exception e) throws RuntimeException {
        super.onException(e);
        Log.d(
            getContext().getClass().getName(),
            "Failed to download answers for veteran: " +veteran.getId() + " rotation: " + rotation.getId()
        );
        Toast.makeText(getContext(), "Failed to download answers", Toast.LENGTH_LONG).show();
      }

      protected void onFinally() {
        if (dialog.isShowing()) {
          dialog.dismiss();
        }
      }

    }.execute();
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.main, menu);
    return super.onCreateOptionsMenu(menu);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.action_about:
        return showAboutActivity();
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  private boolean showAboutActivity() {
    Intent intent = new Intent(this, AboutActivity.class);
    this.startActivity(intent);
    return true;
  }

  private String retrieveAnswerText(final VeteranQuestionAnswer answer) {
    final String label = answer.getAnswer();
    if(label == null || label.isEmpty()) {
      return NO_ANSWER;
    }
    return label;
  }

  private Veteran loadVeteran() {
    int veteranId = getIntent().getIntExtra(VeteranDetailActivity.VETERAN_ID, 0);
    Veteran veteran = new Veteran();
    veteran.setId(veteranId);
    return veteran;
  }

  private Rotation loadRotation() {
    int rotationId = getIntent().getIntExtra(VeteranDetailActivity.ROTATION_ID, 0);
    String surveyTitle = getIntent().getStringExtra(VeteranDetailActivity.SURVEY_TITLE);
    Survey survey = new Survey();
    survey.setTitle(surveyTitle);
    final Rotation rotation = new Rotation();
    rotation.setId(rotationId);
    rotation.setSurvey(survey);
    return rotation;
  }
}
