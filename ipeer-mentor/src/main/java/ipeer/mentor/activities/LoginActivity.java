package ipeer.mentor.activities;

import android.content.*;
import android.os.*;
import android.text.*;
import android.util.*;
import android.view.*;
import android.widget.*;
import com.google.inject.Inject;
import constants.Key;
import ipeer.mentor.R;
import ipeer.util.Client;
import roboguice.activity.RoboActivity;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;
import roboguice.util.RoboAsyncTask;

@ContentView(R.layout.login_activity)
public class LoginActivity extends RoboActivity {

  public static final String USERNAME = "username";

  @Inject
  private Client client;
  @InjectView(R.id.username)
  private TextView textViewUsername;
  @InjectView(R.id.password)
  private TextView textViewPassword;
  @InjectView(R.id.ok)
  private Button buttonOk;
  @InjectView(R.id.cancel)
  private Button buttonCancel;

  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    LinearLayout layout = new LinearLayout(this);
    layout.setOrientation(LinearLayout.VERTICAL);

    final SharedPreferences preferences = getSharedPreferences(Key.PREFERENCE_FILE.name(), MODE_PRIVATE);
    final String preferenceUsername = preferences.getString(USERNAME, null);
    if (TextUtils.isEmpty(preferenceUsername)) {
      textViewUsername.setText("");
    }
    else{
      textViewUsername.setText(preferenceUsername);
      textViewPassword.requestFocus();
    }

    buttonOk.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(final View view) {
        String username = getUsername();
        String password = textViewPassword.getText().toString();
        if (!TextUtils.isEmpty(username) && !TextUtils.isEmpty(password)) {
          login(username, password);
        }
      }
    });

    buttonCancel.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(final View view) {
        finish();
      }
    });
  }

  private String getUsername() {
    if (!TextUtils.isEmpty(textViewUsername.getText())) {
      return textViewUsername.getText().toString();
    }
    return null;
  }

  private void login(final String username, final String password) {
    new RoboAsyncTask<Void>(this) {
      @Override
      public Void call() throws Exception {
        client.configure(username, password);
        client.login();
        return null;
      }

      @Override
      protected void onSuccess(final Void result) throws Exception {
        getSharedPreferences(Key.PREFERENCE_FILE.name(), MODE_PRIVATE).edit().putString(USERNAME, username).commit();
        Intent intent = new Intent(LoginActivity.this, VeteranListActivity.class);
        finish();
        startActivity(intent);
      }

      @Override
      protected void onException(final Exception ex) throws RuntimeException {
        super.onException(ex);
        Log.d(getClass().getName(), "Login failed", ex);
        Toast.makeText(getContext(), "Login failed: " + ex.getMessage(), Toast.LENGTH_LONG).show();

        textViewUsername.requestFocus();
        textViewPassword.setText(null);
        getSharedPreferences(Key.PREFERENCE_FILE.name(), MODE_PRIVATE).edit().putString(USERNAME, null).commit();
      }
    }.execute();
  }
}
