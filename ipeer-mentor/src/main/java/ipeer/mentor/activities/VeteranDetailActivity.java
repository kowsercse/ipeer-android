package ipeer.mentor.activities;

import android.content.*;
import android.graphics.*;
import android.net.*;
import android.os.*;
import android.util.*;
import android.view.*;
import android.view.ViewGroup.*;
import android.widget.*;
import com.androidplot.xy.*;
import com.google.inject.Inject;
import entity.*;
import ipeer.mentor.R;
import ipeer.util.Client;
import ipeer.util.CollectionUtils;
import ipeer.util.LocalDatabaseClient;
import roboguice.activity.RoboActivity;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;
import roboguice.util.RoboAsyncTask;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@ContentView(R.layout.veteran_detail_activity)
public class VeteranDetailActivity extends RoboActivity {
  public static final String VETERAN_ID = "veteranId";
  public static final String ROTATION_ID = "surveyId";
  public static final String SURVEY_TITLE = "surveyTitle";

  @InjectView(R.id.veteranId)
  private TextView textViewId;
  @InjectView(R.id.veteranName)
  private TextView textViewName;
  @InjectView(R.id.veteranPhone)
  private TextView textViewPhone;

  @InjectView(R.id.surveys)
  private ListView listViewSurveys;

  @InjectView(R.id.textMessage)
  private Button buttonTextMessage;
  @InjectView(R.id.call)
  private Button buttonCall;
  @InjectView(R.id.sendEmail)
  private Button buttonEmail;
  @InjectView(R.id.graph)
  private LinearLayout graphViewContainer;

  @Inject
  private Client client;
  private LocalVeteran localVeteran;

  @Override
  protected void onCreate(final Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    downloadRotations();
    localVeteran = getLocalVeteran();

    textViewId.setText(getIdText());
    textViewName.setText(localVeteran.getName());
    textViewPhone.setText(localVeteran.getPhone());

    buttonCall.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(final View v) {
        String phoneCallUri = "tel:" + localVeteran.getPhone();
        Intent CallIntent = new Intent(Intent.ACTION_CALL);
        CallIntent.setData(Uri.parse(phoneCallUri));
        startActivity(CallIntent);
      }
    });
    buttonTextMessage.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(final View v) {
        String uri = "smsto:" + localVeteran.getPhone();
        Intent textIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse(uri));
        textIntent.putExtra("compose_mode", true);
        startActivity(textIntent);
      }
    });
    buttonEmail.setOnClickListener(new View.OnClickListener() {

      @Override
      public void onClick(View view) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("message/rfc822");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{localVeteran.getEmail()});
        intent.putExtra(Intent.EXTRA_SUBJECT, "");
        intent.putExtra(Intent.EXTRA_TEXT, "");
        try {
          startActivity(Intent.createChooser(intent, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
          Toast.makeText(VeteranDetailActivity.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }
      }
    });
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.main, menu);
    return super.onCreateOptionsMenu(menu);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.action_about:
        return showAboutActivity();
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  private boolean showAboutActivity() {
    Intent intent = new Intent(this, AboutActivity.class);
    this.startActivity(intent);
    return true;
  }

  private void downloadSurveyResult() {
    new RoboAsyncTask<List<SurveyResult>>(this) {
      @Override
      public List<SurveyResult> call() throws Exception {
        final Veteran veteran = new Veteran(localVeteran.getId());
        final List<SurveyResult> surveyResults = client.getSurveyResults(veteran);
        return CollectionUtils.isEmpty(surveyResults) ? Collections.<SurveyResult>emptyList() : surveyResults;
      }

      @Override
      protected void onSuccess(final List<SurveyResult> surveyResults) throws Exception {
        showGraph(surveyResults);
      }
    }.execute();
  }

  private void showGraph(final List<SurveyResult> surveyResults) {
    final LayoutParams layoutParams = new LayoutParams(graphViewContainer.getWidth(), graphViewContainer.getHeight());
    XYPlot plot = new XYPlot(this, "");
    plot.setLayoutParams(layoutParams);
    plot.setRangeBoundaries(0, 3, BoundaryMode.FIXED);
    plot.setRangeStepValue(4);
    plot.getLayoutManager().remove(plot.getLegendWidget());
    plot.setDomainStep(XYStepMode.SUBDIVIDE, 1);

    List<Float> scores = new ArrayList<Float>();
    for (SurveyResult surveyResult : surveyResults) {
      scores.add(surveyResult.getAverageScore());
    }
    LineAndPointFormatter formatter = new LineAndPointFormatter();
    formatter.configure(getApplicationContext(), R.xml.line_point_formatter);

    XYSeries series = new SimpleXYSeries(scores, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, null);
    plot.addSeries(series, formatter);
    graphViewContainer.addView(plot);
  }

  private void downloadRotations() {
    new RoboAsyncTask<List<Rotation>>(this) {
      @Override
      public List<Rotation> call() throws Exception {
        final Veteran veteran = new Veteran();
        veteran.setId(localVeteran.getId());
        return client.getRotations(veteran);
      }

      @Override
      protected void onSuccess(final List<Rotation> rotations) throws Exception {
        downloadSurveyResult();
        ListAdapter adapter = new ArrayAdapter<Rotation>(VeteranDetailActivity.this, R.layout.rotation_summary_layout, rotations) {
          @Override
          public View getView(final int position, final View convertView, final ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.rotation_summary_layout, parent, false);
            final Rotation rotation = getItem(position);
            final Survey survey = rotation.getSurvey();
            rowView.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(final View view) {
                Intent intent = new Intent(VeteranDetailActivity.this, SurveyAnswerActivity.class);
                intent.putExtra(VeteranDetailActivity.VETERAN_ID, localVeteran.getId());
                intent.putExtra(VeteranDetailActivity.ROTATION_ID, rotation.getId());
                intent.putExtra(VeteranDetailActivity.SURVEY_TITLE, survey.getTitle());
                startActivity(intent);
              }
            });

            final TextView surveyDateTextView = (TextView) rowView.findViewById(R.id.survey_date);
            surveyDateTextView.setText(getStartTime(rotation));

            final TextView surveyTitleTextView = (TextView) rowView.findViewById(R.id.survey_title);
            surveyTitleTextView.setPaintFlags(surveyTitleTextView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            surveyTitleTextView.setText(survey.getTitle());

            return rowView;
          }
        };

        listViewSurveys.setAdapter(adapter);
      }

      @Override
      protected void onException(final Exception e) throws RuntimeException {
        super.onException(e);
        Toast.makeText(getContext(), "Failed to download surveys", Toast.LENGTH_LONG).show();
      }
    }.execute();
  }

  private LocalVeteran getLocalVeteran() {
    LocalDatabaseClient databaseClient = new LocalDatabaseClient(this);
    int veteranId = getIntent().getIntExtra(VETERAN_ID, 0);
    LocalVeteran localVeteran = databaseClient.getVeteran(veteranId);
    if(localVeteran == null) {
      localVeteran = new LocalVeteran(veteranId);
    }
    return localVeteran;
  }

  private String getStartTime(final Rotation rotation) {
    String startTime = rotation.getStartTime();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    try {
      final Date parse = simpleDateFormat.parse(startTime);
      SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("MMM d ''yy");
      startTime = simpleDateFormat2.format(parse);
    } catch (java.text.ParseException e) {
      Log.d(getClass().getName(), "Failed to parse as date: " + startTime, e);
    }
    return startTime;
  }

  private String getIdText() {
    return String.format("Participant Id: %02d", localVeteran.getId());
  }

}
