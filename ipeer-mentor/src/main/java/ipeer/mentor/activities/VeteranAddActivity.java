package ipeer.mentor.activities;

import android.app.*;
import android.content.*;
import android.os.*;
import android.view.*;
import android.widget.*;
import com.google.inject.Inject;
import entity.LocalVeteran;
import ipeer.mentor.R;
import ipeer.mentor.task.AddVeteranTask;
import ipeer.util.Application;
import ipeer.util.Client;
import ipeer.util.LocalDatabaseClient;
import roboguice.activity.RoboActivity;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

@ContentView(R.layout.veteran_add_layout)
public class VeteranAddActivity extends RoboActivity {

  private LocalDatabaseClient localClient = new LocalDatabaseClient(this);
  @Inject
  private Application application;
  @Inject
  private Client client;

  @InjectView(R.id.name_text_view)
  private TextView nameTextView;
  @InjectView(R.id.phone_text_view)
  private TextView phoneTextView;
  @InjectView(R.id.email_text_view)
  private TextView emailTextView;
  @InjectView(R.id.save_button)
  private Button saveButton;
  @InjectView(R.id.cancel_button)
  private Button cancelButton;

  @Override
  protected void onCreate(final Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    saveButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(final View v) {
        final LocalVeteran localVeteran = new LocalVeteran();
        localVeteran.setName(nameTextView.getText().toString());
        localVeteran.setPhone(phoneTextView.getText().toString());
        localVeteran.setEmail(emailTextView.getText().toString());
        new AddVeteranTask(VeteranAddActivity.this, application, client, localClient, localVeteran, new AddVeteranTask.Callback() {
          @Override
          public void call(final LocalVeteran veteran) {
            final String message = String.format("Please write the username: %d on the demographics form", localVeteran.getId());
            new AlertDialog.Builder(VeteranAddActivity.this)
                .setTitle(localVeteran.getName() +  " is added as veteran")
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                  public void onClick(DialogInterface dialog, int which) {
                    finish();
                  }
                })
                .setIcon(android.R.drawable.ic_menu_info_details)
                .show();
          }
        }).execute();
      }
    });

    cancelButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(final View v) {
        finish();
      }
    });
  }
}
