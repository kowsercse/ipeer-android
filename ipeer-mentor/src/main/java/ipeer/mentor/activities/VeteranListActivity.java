package ipeer.mentor.activities;

import android.content.*;
import android.os.*;
import android.view.*;
import android.widget.*;
import com.google.inject.Inject;
import entity.Veteran;
import ipeer.mentor.R;
import ipeer.mentor.component.VeteranArrayAdapter;
import ipeer.mentor.task.DownloadVeteransTask;
import ipeer.util.Application;
import ipeer.util.Client;
import ipeer.util.LocalDatabaseClient;
import roboguice.activity.RoboActivity;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

@ContentView(R.layout.veteran_list)
public class VeteranListActivity extends RoboActivity {

  @Inject
  private Client client;
  @Inject
  private Application application;
  private LocalDatabaseClient localClient;

  @InjectView(R.id.veteran_list)
  private ListView listViewVeterans;
  @InjectView(R.id.veteran_add)
  private Button buttonVeteranAdd;

  private boolean doubleBackToExitPressedOnce;
  private ArrayAdapter<Veteran> adapter;

  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    localClient = new LocalDatabaseClient(this);

    adapter = new VeteranArrayAdapter(this, localClient);
    listViewVeterans.setAdapter(adapter);
    buttonVeteranAdd.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(final View view) {
        Intent intent = new Intent(VeteranListActivity.this, VeteranAddActivity.class);
        startActivityForResult(intent, 0);
      }
    });

    new DownloadVeteransTask(this, application, client, new DownloadVeteransTask.Callback() {
      @Override
      public void call() {
        reset();
      }
    }).execute();
  }

  @Override
  public void onBackPressed() {
    if (doubleBackToExitPressedOnce) {
      finish();
    }
    else {
      this.doubleBackToExitPressedOnce = true;
      Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
      new Handler().postDelayed(new Runnable() {
        @Override
        public void run() {
          doubleBackToExitPressedOnce=false;
        }
      }, 2000);
    }
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.main, menu);
    return super.onCreateOptionsMenu(menu);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.action_about:
        return showAboutActivity();
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  private boolean showAboutActivity() {
    Intent intent = new Intent(this, AboutActivity.class);
    this.startActivity(intent);
    return true;
  }

  @Override
  protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    reset();
  }

  public void reset() {
    adapter.clear();
    for (Veteran veteran : application.getVeterans()) {
      adapter.add(veteran);
    }
  }

}
