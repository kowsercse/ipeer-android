package ipeer.mentor;

import android.app.*;
import android.content.*;
import android.net.*;
import ipeer.mentor.service.DailyAlarmBroadcastReceiver;
import service.ConnectivityListener;

public class MentorApplication extends Application {

  @Override
  public void onCreate() {
    super.onCreate();

    DailyAlarmBroadcastReceiver.registerDailyAlarmListener(this);
    ConnectivityListener connectivityListener = new ConnectivityListener();
    registerReceiver(connectivityListener, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
  }

}
