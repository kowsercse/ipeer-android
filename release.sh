#!/bin/bash
# we would pass the property name and property file name as a parameter to this function
#from where we want to read the property
# @param propertyName
# @param fileName
function getPropertyFromFile()
{
  propertyName=$(echo $2 | sed -e 's/\./\\\./g')
  fileName=$2;
  cat $1 | sed -n -e "s/^[ ]*//g;/^#/d;s/^$propertyName=//p" | tail -1
}

environment=$1
source ~/.ipeer-android/$environment.properties

version_major=$(getPropertyFromFile ipeer-lib/src/main/resources/config.properties ipeer.config.version.major)
version_minor=$(getPropertyFromFile ipeer-lib/src/main/resources/config.properties ipeer.config.version.minor)

version_build=$(date +"%Y%m%d%H%M%S")
echo -e "ipeer.config.url.base=${base_url}\nipeer.config.version.build=${version_build}\n" > ipeer-lib/src/main/resources/$environment.properties

xml="<?xml version=\"1.0\"?>
<verion>
    <major>${version_major}</major>
    <minor>${version_minor}</minor>
    <buildNumber>${version_build}</buildNumber>
    <enabled>true</enabled>
</verion>"

echo $xml | xmllint --format -

mvn clean install
location=$(curl -v -i -H "Authorization: ${secret_login}" -d "${xml}" -X POST ${base_url}/version/collection \
          | tr '\b\r' '\n' \
          | grep -i "X-APK-Location" \
          | sed s/"X-APK-Location: "//)

if [ "${location}" ]; then
   echo ${location}
   curl -i -X POST \
      -H "Authorization: ${login_secret}" \
      -H "Content-Type: multipart/form-data;" \
      -F mentorApk=@ipeer-mentor/target/ipeer-mentor-trunk-SNAPSHOT.apk \
      -F menteeApk=@ipeer-mentee/target/ipeer-mentee-trunk-SNAPSHOT.apk \
      ${location}
fi

rm ipeer-lib/src/main/resources/$environment.properties
