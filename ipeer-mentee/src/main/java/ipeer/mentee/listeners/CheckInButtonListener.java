package ipeer.mentee.listeners;

import android.app.*;
import android.content.*;
import android.view.*;
import entity.Rotation;
import ipeer.mentee.activities.LatestSurveyActivity;
import ipeer.mentee.activities.QuestionsActivity;

import java.text.ParseException;

public class CheckInButtonListener implements View.OnClickListener {
  private LatestSurveyActivity latestSurveyActivity;
  private Rotation rotation;
  private final String nextDay;

  public CheckInButtonListener(final LatestSurveyActivity latestSurveyActivity, final Rotation rotation, final boolean firstCheckIn) throws ParseException {
    this.latestSurveyActivity = latestSurveyActivity;
    this.rotation = rotation;
    nextDay = !firstCheckIn ? "Monday" : "Friday";
  }

  public void onClick(final View view) {
    if(rotation != null && rotation.isActive()) {
      Intent intent = new Intent(latestSurveyActivity, QuestionsActivity.class);
      intent.putExtra(LatestSurveyActivity.LAST_SURVEY, rotation.getSurvey().getId());
      intent.putExtra(LatestSurveyActivity.ROTATION, rotation.getId());

      latestSurveyActivity.startActivityForResult(intent, LatestSurveyActivity.REQUEST_CODE);
    }
    else {
      AlertDialog.Builder builder = new AlertDialog.Builder(latestSurveyActivity);
      builder.setMessage("This check in is not available yet. Check back in on " + nextDay);
      builder.show();
    }
  }
}
