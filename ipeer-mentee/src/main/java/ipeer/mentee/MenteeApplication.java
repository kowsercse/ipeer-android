package ipeer.mentee;

import android.app.*;
import android.content.*;
import android.net.*;
import ipeer.mentee.service.DailyAlarmBroadcastReceiver;
import service.ConnectivityListener;

public class MenteeApplication extends Application {
  @Override
  public void onCreate() {
    super.onCreate();

    DailyAlarmBroadcastReceiver.registerDailyAlarmListener(this);
    ConnectivityListener connectivityListener = new ConnectivityListener();
    registerReceiver(connectivityListener, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
  }
}
