package ipeer.mentee.task;


import android.app.*;
import android.util.*;
import android.widget.*;
import entity.Mentor;
import ipeer.mentee.activities.LatestSurveyActivity;
import ipeer.util.Application;
import ipeer.util.Client;
import roboguice.util.RoboAsyncTask;

public class DownloadMentorTask extends RoboAsyncTask<Mentor> {
  private final LatestSurveyActivity latestSurveyActivity;
  private Application application;
  private ProgressDialog progressDialog;
  private Client client;

  public DownloadMentorTask(final LatestSurveyActivity latestSurveyActivity, Application application, final ProgressDialog dialog, final Client client) {
    super(latestSurveyActivity);
    this.latestSurveyActivity = latestSurveyActivity;
    this.application = application;
    this.progressDialog = dialog;
    this.client = client;
  }

  @Override
  public void onPreExecute() {
    progressDialog.setMessage("Downloading Surveys");
    progressDialog.show();
  }

  @Override
  public Mentor call() throws Exception {
    return client.getMentor();
  }

  @Override
  protected void onSuccess(final Mentor mentor) throws Exception {
    application.setMentor(mentor);
    Log.d(getClass().getName(), mentor.getFirstName());
  }

  @Override
  protected void onException(final Exception e) throws RuntimeException {
    super.onException(e);
    Log.d(getClass().getName(), e.getMessage(), e);
    Toast.makeText(getContext(), "Failed to download mentor information " + e.getMessage(), Toast.LENGTH_LONG).show();
  }

  @Override
  protected void onFinally() throws RuntimeException {
    latestSurveyActivity.downloadAllRotations();
  }
}
