package ipeer.mentee.task;

import android.app.*;
import android.content.*;
import android.os.*;
import android.util.*;
import android.widget.*;
import constants.Key;
import entity.Rotation;
import entity.VeteranAnswer;
import ipeer.mentee.activities.QuestionsActivity;
import ipeer.util.Client;
import ipeer.util.DateUtility;
import roboguice.util.RoboAsyncTask;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class SubmitAnswerTask extends RoboAsyncTask<Void> {
  private final List<VeteranAnswer> veteranAnswers;
  protected ProgressDialog dialog;
  private QuestionsActivity questionsActivity;
  private Client client;
  private Rotation rotation;

  public SubmitAnswerTask(final QuestionsActivity questionsActivity, final Client client, final Rotation rotation, final List<VeteranAnswer> veteranAnswers) {
    super(questionsActivity);
    this.veteranAnswers = veteranAnswers;
    dialog = new ProgressDialog(questionsActivity);
    this.questionsActivity = questionsActivity;
    this.client = client;
    this.rotation = rotation;
  }

  @Override
  protected void onPreExecute() {
    dialog.setMessage("Submitting Answers");
    dialog.show();
  }

  @Override
  public Void call() throws Exception {
    client.submitVeteranAnswers(veteranAnswers, rotation);
    return null;
  }

  @Override
  protected void onSuccess(final Void aVoid) throws Exception {
    final Date time = Calendar.getInstance().getTime();
    final SharedPreferences preferences = questionsActivity.getSharedPreferences(Key.PREFERENCE_FILE.name(), Activity.MODE_PRIVATE);
    preferences.edit().putString(Key.SUBMISSION_DATE.name(), DateUtility.formatDate(time)).commit();

    Toast.makeText(getContext(), "Survey Data Submitted Successfully", Toast.LENGTH_LONG).show();
    new Handler().postDelayed(new Runnable() {
      @Override
      public void run() {
        Intent returnIntent = new Intent();
        questionsActivity.setResult(Activity.RESULT_OK, returnIntent);
        questionsActivity.finish();
      }
    }, Toast.LENGTH_LONG);
  }

  @Override
  protected void onException(final Exception e) throws RuntimeException {
    super.onException(e);
    Log.d(getClass().getName(), e.getMessage(), e);
    Toast.makeText(getContext(), "Failed to submit answer: " + e.getMessage(), Toast.LENGTH_LONG).show();
  }

  protected void onFinally() {
    if (dialog.isShowing()) {
      dialog.dismiss();
    }
  }

}
