package ipeer.mentee.service;

import android.app.*;
import android.content.*;
import android.net.*;
import android.util.*;
import constants.Key;
import ipeer.mentee.R;
import ipeer.mentee.activities.LoginActivity;
import ipeer.util.*;
import ipeer.util.Config;
import roboguice.util.RoboAsyncTask;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.Calendar;
import java.util.Date;

public class DailyAlarmBroadcastReceiver extends BroadcastReceiver {
  public static final String APK = "mentee";

  private final Client client;

  public DailyAlarmBroadcastReceiver() {
    client = new Client();
  }

  @Override
  public void onReceive(final Context context, final Intent intent) {
    downloadUpdateAndNotify(context);
    showNewSurveyNotification(context);
  }

  private void downloadUpdateAndNotify(final Context context) {
    new RoboAsyncTask<File>(context) {
      @Override
      public File call() throws Exception {
        File file = null;
        try {
          final URI uri = client.checkForUpdate();
          if(uri != null) {
            Log.d(getClass().getName(), "New update available.");
            file = client.downloadFile(uri, APK);
          }
        }
        catch (IOException e) {
          Log.d(getClass().getName(), "Error downloading latest update");
        }
        return file;
      }

      @Override
      protected void onSuccess(final File file) throws Exception {
        super.onSuccess(file);
        if(file != null) {
          showInstallNotification(context, file);
        }
      }
    }.execute();
  }

  private void showInstallNotification(final Context context, final File apk) {
    final Intent intent = promptInstall(apk);
    final PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
    final Notification notification = new Notification.Builder(context)
        .setSmallIcon(R.drawable.ic_launcher)
        .setContentTitle("New update downloaded")
        .setContentText(apk.getName() + " is ready to be installed")
        .setAutoCancel(false)
        .setContentIntent(pendingIntent)
        .getNotification();
    NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    notificationManager.notify(0, notification);
  }

  private Intent promptInstall(File outputPath) {
    return new Intent(Intent.ACTION_VIEW)
        .setDataAndType(Uri.fromFile(outputPath), "application/vnd.android.package-archive")
        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
  }

  private void showNewSurveyNotification(final Context context) {
    final SharedPreferences preferences = context.getSharedPreferences(Key.PREFERENCE_FILE.name(), Context.MODE_PRIVATE);
    final String submissionTime = preferences.getString(Key.SUBMISSION_DATE.name(), null);
    if(isSurveyDate() && !isSurveySubmitted(submissionTime)) {
      final Intent intent = new Intent(context, LoginActivity.class);
      final PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
      final Notification notification = new Notification.Builder(context)
          .setSmallIcon(R.drawable.ic_launcher)
          .setContentTitle("New survey available")
          .setContentText("Please complete the survey")
          .setAutoCancel(false)
          .setContentIntent(pendingIntent)
          .getNotification();
      NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
      notificationManager.notify(0, notification);
    }
  }

  private boolean isSurveyDate() {
    final int today = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
    return today == Calendar.MONDAY
        || today == Calendar.TUESDAY
        || today == Calendar.FRIDAY
        || today == Calendar.SATURDAY;
  }

  private boolean isSurveySubmitted(final String submissionTime) {
    if(submissionTime == null || submissionTime.length() == 0) {
      return false;
    }

    final Date parsedSubmissionTime = DateUtility.parseDate(submissionTime);
    if (parsedSubmissionTime == null) {
      return false;
    }

    final Date today = Calendar.getInstance().getTime();
    if(today.after(DateUtility.getDateFor(Calendar.FRIDAY))) {
      if(parsedSubmissionTime.after(DateUtility.getDateFor(Calendar.FRIDAY))) {
        return true;
      }
    }
    if(today.after(DateUtility.getDateFor(Calendar.MONDAY))) {
      if(parsedSubmissionTime.after(DateUtility.getDateFor(Calendar.MONDAY))) {
        return true;
      }
    }

    return false;
  }

  public static void registerDailyAlarmListener(final Context context) {
    Intent downloadAlarmReceiverIntent = new Intent(context, DailyAlarmBroadcastReceiver.class);
    final PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, downloadAlarmReceiverIntent, PendingIntent.FLAG_UPDATE_CURRENT);

    long triggerTimeInMillis = getTriggerTimeInMillis();
    AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
    alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, triggerTimeInMillis, Config.getInstance().getUpdateInterval(), pendingIntent);
  }

  private static long getTriggerTimeInMillis() {
    Calendar calendar = Calendar.getInstance();
    calendar.setTimeInMillis(System.currentTimeMillis());
    calendar.set(Calendar.HOUR_OF_DAY, 0);
    calendar.set(Calendar.MINUTE, 1);
    return calendar.getTimeInMillis();
  }

}
