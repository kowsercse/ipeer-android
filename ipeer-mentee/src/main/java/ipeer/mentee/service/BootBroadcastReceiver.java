package ipeer.mentee.service;

import android.content.*;
import android.util.*;

public class BootBroadcastReceiver extends BroadcastReceiver {

  @Override
  public void onReceive(final Context context, final Intent intent) {
    Log.d(getClass().getName(), "invoked onReceive " + getClass().getName());
    DailyAlarmBroadcastReceiver.registerDailyAlarmListener(context);
  }

}
