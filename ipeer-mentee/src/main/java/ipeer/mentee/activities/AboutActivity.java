package ipeer.mentee.activities;

import android.content.*;
import android.graphics.*;
import android.os.*;
import android.view.*;
import android.widget.*;
import ipeer.mentee.R;
import ipeer.util.Config;
import roboguice.activity.RoboActivity;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

import java.util.Arrays;
import java.util.List;

@ContentView(R.layout.about_layout)
public class AboutActivity extends RoboActivity {
  private static final List<String> LINK_LABELS = Arrays.asList("Home", "Issue", "Documentation");
  private static final List<String> LINKS = Arrays.asList(
      "http://dryhootch.org/",
      "Report an issue",
      "http://ipeer.cloudapp.net/testpeer/web/app.php/"
  );

  @InjectView(R.id.links)
  private ListView linksListView;

  @InjectView(R.id.versionListView)
  private ListView versionListView;

  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    linksListView.setAdapter(createLinksAdapter());
    versionListView.setAdapter(createVersionViewAdapter());
  }

  public ListAdapter createLinksAdapter() {
    return new ArrayAdapter<Void>(this, R.layout.about_row_view, new Void[LINKS.size()]) {
      @Override
      public View getView(final int position, final View convertView, final ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.about_row_view, parent, false);
        TextView label = (TextView) rowView.findViewById(R.id.label);
        TextView linkView = (TextView) rowView.findViewById(R.id.value);
        linkView.setPaintFlags(linkView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        label.setText(LINK_LABELS.get(position));
        linkView.setText(LINKS.get(position));

        if(position == 1) {
          linkView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
              Intent browserIntent = new Intent(Intent.ACTION_VIEW, Config.getInstance().getDocumentationUrl());
              startActivity(browserIntent);
            }
          });
        }

        return rowView;
      }
    };
  }

  private ListAdapter createVersionViewAdapter() {
    return new ArrayAdapter<Void>(this, R.layout.about_row_view, new Void[1]) {
      @Override
      public View getView(final int position, final View convertView, final ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.about_row_view, parent, false);

        TextView label = (TextView) rowView.findViewById(R.id.label);
        TextView linkView = (TextView) rowView.findViewById(R.id.value);
        if(position == 0) {
          final Config instance = Config.getInstance();
          label.setText("Version");
          linkView.setText(String.format("%s.%s-%s", instance.getMajorVersion(), instance.getMinorVersion(), instance.getBuildVersion()));
          linkView.setTypeface(null, Typeface.BOLD);
        }

        return rowView;
      }
    };
  }
}
