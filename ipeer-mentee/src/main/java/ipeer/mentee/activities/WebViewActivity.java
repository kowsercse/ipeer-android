package ipeer.mentee.activities;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

import ipeer.mentee.R;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

public class WebViewActivity extends Activity {

  private WebView webView;

  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.webview_layout);
    webView = (WebView) findViewById(R.id.webView);
    webView.getSettings().setJavaScriptEnabled(true);
    webView.loadUrl("http://dryhootch.org");

  }

}
