package ipeer.mentee.activities;

import android.app.*;
import android.content.*;
import android.os.*;
import android.util.*;
import android.view.*;
import android.widget.*;
import com.google.inject.Inject;
import entity.*;
import ipeer.mentee.R;
import ipeer.mentee.task.SubmitAnswerTask;
import ipeer.util.Client;
import roboguice.activity.RoboActivity;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;
import roboguice.util.RoboAsyncTask;

import java.util.ArrayList;
import java.util.List;

@ContentView(R.layout.survey_view_layout)
public class QuestionsActivity extends RoboActivity {
  @Inject
  private Client client;

  private Survey survey;
  private List<Question> questions;
  private List<Integer> answers;
  private int currentQuestion;
  private List<Integer> selectedAnswerIndices;

  @InjectView(R.id.txtSurveyName)
  private TextView txtSurveyName;
  @InjectView(R.id.question_number)
  private TextView txtQuestionNumber;
  @InjectView(R.id.question_name)
  private TextView txtQuestionName;

  @InjectView(R.id.btn_previous)
  Button btnPrevious;
  @InjectView(R.id.btn_next)
  Button btnNext;

  @InjectView(R.id.question_answers)
  ListView lvAnswers;

  Rotation rotation;

  public QuestionsActivity() {
    survey = new Survey();
    currentQuestion = 1;
  }

  @Override
  protected void onCreate(final Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    txtSurveyName.setText("Dryhootch Mentor Survey"); //later we should get the actual survey name

    btnPrevious.setOnClickListener(new View.OnClickListener() {
      public void onClick(View v) {
        if (currentQuestion > 1) {
          currentQuestion--;
          displayQuestion(currentQuestion);
        }
      }
    });

    btnNext.setOnClickListener(new View.OnClickListener() {
      public void onClick(View v) {
        if (currentQuestion < questions.size()) {
          currentQuestion++;
          displayQuestion(currentQuestion);
        } else try {
          submitData();
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    });
    rotation = new Rotation();
    survey.setId(getIntent().getIntExtra(LatestSurveyActivity.LAST_SURVEY, 0));
    rotation.setId(getIntent().getIntExtra(LatestSurveyActivity.ROTATION, 0));
    downloadSurveyQuestionsTask();
  }

  private void submitData() throws Exception {
    List<VeteranAnswer> veteranAnswers = new ArrayList<VeteranAnswer>();
    int i = 0;
    for (int answer : answers) {
      VeteranAnswer veteranAnswer = new VeteranAnswer();
      veteranAnswer.setQuestion(questions.get(i).getId());
      veteranAnswer.setAnswer(answer);
      veteranAnswers.add(veteranAnswer);
      i++;
    }
    if (!veteranAnswers.isEmpty()) {
      new SubmitAnswerTask(this, client, rotation, veteranAnswers).execute();
    }
  }

  private void displayQuestion(int questionNumber) {
    final Question question = questions.get(questionNumber - 1);
    AnswerAdapter answerAdapter = new AnswerAdapter(this, R.layout.answer_row_layout, question.getAnswers());
    lvAnswers.setAdapter(answerAdapter);
    answerAdapter.checkItem(selectedAnswerIndices.get(currentQuestion - 1));

    txtQuestionNumber.setText("Question" + questionNumber + " of " + questions.size());
    txtQuestionName.setText(questionNumber + ". " + question.getTitle());

    if (questionNumber == questions.size()) btnNext.setText("Submit");
    else btnNext.setText("Next Question >");
    if (questionNumber == 1) btnPrevious.setEnabled(false);
    else btnPrevious.setEnabled(true);
  }

  private void downloadSurveyQuestionsTask() {
    new RoboAsyncTask<List<Question>>(this) {
      protected ProgressDialog dialog = new ProgressDialog(QuestionsActivity.this);

      @Override
      protected void onPreExecute() {
        dialog.setMessage("Retrieving questions");
        dialog.show();
      }

      @Override
      public List<Question> call() throws Exception {
        return client.getSurveyQuestions(survey);
      }

      @Override
      protected void onSuccess(final List<Question> questionList) throws Exception {
        questions = questionList;
        answers = new ArrayList<Integer>();
        selectedAnswerIndices = new ArrayList<Integer>();
        for (int i = 0; i < questions.size(); i++) {
          answers.add(0);
          selectedAnswerIndices.add(-1);
        }
        displayQuestion(currentQuestion);
      }

      @Override
      protected void onException(final Exception e) throws RuntimeException {
        super.onException(e);
        Log.d(getClass().getName(), e.getMessage(), e);
        Toast.makeText(getContext(), "Failed to download latest survey" + e.getMessage(), Toast.LENGTH_LONG).show();
      }

      protected void onFinally() {
        if (dialog.isShowing()) {
          dialog.dismiss();
        }
      }

    }.execute();
  }

  public class AnswerAdapter extends ArrayAdapter<Answer> {
    private List<Answer> allAnswers;

    public AnswerAdapter(Context context, int textViewResourceId, List<Answer> answers) {
      super(context, R.layout.answer_row_layout, answers);
      allAnswers = answers;
    }

    @Override
    public View getView(final int position, final View convertView, final ViewGroup parent) {
      LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      View rowView = inflater.inflate(R.layout.answer_row_layout, parent, false);
      final Answer answer = getItem(position);
      TextView tvAnswerText = (TextView) rowView.findViewById(R.id.tvAnswerText);
      tvAnswerText.setText(answer.getLabel());
      rowView.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(final View view) {
          answers.set(currentQuestion - 1, answer.getId());
          checkItem(position);
        }
      });
      return rowView;
    }

    @Override
    public int getCount() {
      return allAnswers != null ? allAnswers.size() : 0;
    }

    private void checkItem(int positionToCheck) {
      if (positionToCheck >= 0) {
        for (int i = 0; i < getCount(); i++) {
          View rowView = lvAnswers.getChildAt(i);
          if (rowView != null) {
            final TextView tvSelector = (TextView) rowView.findViewById(R.id.tvSelector);
            if (i != positionToCheck) {
              tvSelector.setText("");
            } else {
              tvSelector.setText("X");
              selectedAnswerIndices.set(currentQuestion - 1, positionToCheck);
            }
          }
        }
      }

    }
  }
}
