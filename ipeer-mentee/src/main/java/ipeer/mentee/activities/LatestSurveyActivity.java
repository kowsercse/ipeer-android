package ipeer.mentee.activities;

import android.app.*;
import android.content.*;
import android.graphics.*;
import android.net.*;
import android.os.*;
import android.util.*;
import android.view.*;
import android.widget.*;
import com.google.inject.Inject;
import entity.Rotation;
import entity.Survey;
import entity.SurveyResult;
import entity.Veteran;
import ipeer.mentee.R;
import ipeer.mentee.listeners.CheckInButtonListener;
import ipeer.mentee.task.DownloadMentorTask;
import ipeer.util.Application;
import ipeer.util.Client;
import ipeer.util.CollectionUtils;
import roboguice.activity.RoboActivity;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;
import roboguice.util.RoboAsyncTask;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@ContentView(R.layout.latest_survey_activity)
public class LatestSurveyActivity extends RoboActivity {
  private static final int BLUE = Color.parseColor("#0047B2");
  private static final int YELLOW = Color.parseColor("#C3872D");
  private static final int GREEN = Color.parseColor("#006221");
  private static final int RED = Color.parseColor("#A30000");
  public static final int REQUEST_CODE = 1;

  public static final String LAST_SURVEY = "LAST_SURVEY";
  public static final String ROTATION = "ROTATION";
  public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
  @Inject
  private Client client;
  @Inject
  private Application application;

  @InjectView(R.id.callMentorTextView)
  private TextView callMentorTextView;
  @InjectView(R.id.textMentorTextView)
  private TextView textMentorTextView;

  @InjectView(R.id.progressBar)
  private ProgressBar progressBar;
  @InjectView(R.id.progressTextView)
  private TextView progressTextView;
  @InjectView(R.id.surveyList)
  private ListView surveyListView;

  @InjectView(R.id.btnCheckInOne)
  private Button btnCheckInOne;
  @InjectView(R.id.btnCheckInTwo)
  private Button btnCheckInTwo;

  @InjectView(R.id.sunday)
  private View sunday;
  @InjectView(R.id.monday)
  private View monday;
  @InjectView(R.id.tuesday)
  private View tuesday;
  @InjectView(R.id.wednesday)
  private View wednesday;
  @InjectView(R.id.thursday)
  private View thursday;
  @InjectView(R.id.friday)
  private View friday;
  @InjectView(R.id.saturday)
  private View saturday;

  private boolean doubleBackToExitPressedOnce;

  protected ProgressDialog dialog;

  private int day;

  private Rotation firstCheckInRotation;
  private Rotation secondCheckInRotation;

  private SurveyResult surveyOneResult;
  private SurveyResult surveyTwoResult;
  private List<Rotation> rotations;

  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    dialog = new ProgressDialog(LatestSurveyActivity.this);
    day = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
    new DownloadMentorTask(this, application, this.dialog, this.client).execute();

    initializeListeners();
  }

  private void downloadSurveyResults() {
    new RoboAsyncTask<List<SurveyResult>>(this) {
      @Override
      public List<SurveyResult> call() throws Exception {
        final Veteran veteran = application.getVeteran();
        final List<SurveyResult> surveyResults = client.getSurveyResults(veteran);
        return CollectionUtils.isEmpty(surveyResults) ? Collections.<SurveyResult>emptyList() : surveyResults;
      }

      @Override
      protected void onSuccess(final List<SurveyResult> surveyResults) throws Exception {
        final Map<Integer, Rotation> rotationMap = new HashMap<Integer, Rotation>(rotations.size());
        for (Rotation rotation : rotations) {
          rotationMap.put(rotation.getId(), rotation);
        }

        ListAdapter adapter = new ArrayAdapter<SurveyResult>(LatestSurveyActivity.this, R.layout.rotation_summary_layout, surveyResults) {
          @Override
          public View getView(final int position, final View convertView, final ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.rotation_summary_layout, parent, false);
            final SurveyResult surveyResult = getItem(position);

            final Rotation rotation = rotationMap.get(surveyResult.getRotationId());
            final Survey survey = rotation.getSurvey();
            final TextView surveyDateTextView = (TextView) rowView.findViewById(R.id.survey_date);
            surveyDateTextView.setText(getStartTime(rotation));

            final TextView surveyTitleTextView = (TextView) rowView.findViewById(R.id.survey_title);
            surveyTitleTextView.setText(survey.getTitle());

            return rowView;
          }
        };

        surveyListView.setAdapter(adapter);
        progressBar.setProgress(surveyResults.size());
        progressBar.setMax(12);
        progressTextView.setText("" + surveyResults.size() + "/12");
      }
    }.execute();
  }

  private void initializeListeners() {
    callMentorTextView.setOnClickListener(new View.OnClickListener() {

      @Override
      public void onClick(View view) {
        String phoneCallUri = "tel:" + application.getMentor().getPhoneNumber();
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse(phoneCallUri));
        LatestSurveyActivity.this.startActivity(callIntent);
      }
    });

    textMentorTextView.setOnClickListener(new View.OnClickListener() {

      @Override
      public void onClick(View view) {
        String uri = "smsto:" + application.getMentor().getPhoneNumber();
        Intent textIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse(uri));
        textIntent.putExtra("compose_mode", true);
        LatestSurveyActivity.this.startActivity(textIntent);
      }
    });
  }

  @Override
  public void onBackPressed() {
    if (doubleBackToExitPressedOnce) {
      finish();
    } else {
      this.doubleBackToExitPressedOnce = true;
      Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
      new Handler().postDelayed(new Runnable() {
        @Override
        public void run() {
          doubleBackToExitPressedOnce = false;
        }
      }, 2000);
    }
  }

  public void onActivityResult(int requestCode, int resultCode, Intent intent) {
    if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {
      downloadAllRotations();
    }
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.main, menu);
    return super.onCreateOptionsMenu(menu);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.action_about:
        return showAboutActivity();
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  private boolean showAboutActivity() {
    Intent intent = new Intent(this, AboutActivity.class);
    this.startActivity(intent);
    return true;
  }

  public void downloadAllRotations() {
    new RoboAsyncTask<List<Rotation>>(this) {

      @Override
      public List<Rotation> call() throws Exception {
        return client.getRotations();
      }

      @Override
      protected void onSuccess(final List<Rotation> downloadedRotations) throws Exception {
        rotations = downloadedRotations;
        initializeRotations();
      }

      @Override
      protected void onException(final Exception e) throws RuntimeException {
        super.onException(e);
        Log.d(getClass().getName(), e.getMessage(), e);
        Toast.makeText(getContext(), "Failed to download latest surveys" + e.getMessage(), Toast.LENGTH_LONG).show();
      }

      protected void onFinally() {
        downloadVeteranSurveyOneResults();
        downloadSurveyResults();
      }

    }.execute();
  }

  private void initializeRotations() throws ParseException {
    final Date firstCheckInDate = getDateFor(Calendar.MONDAY);
    final Date secondCheckInDate = getDateFor(Calendar.FRIDAY);

    for (Rotation rotation : rotations) {
      SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT);
      final Date startTime = simpleDateFormat.parse(rotation.getStartTime());
      final Date endTime = simpleDateFormat.parse(rotation.getEndTime());
      if(firstCheckInDate.after(startTime) && firstCheckInDate.before(endTime)) {
        firstCheckInRotation = rotation;
      }
      else if(secondCheckInDate.after(startTime) && secondCheckInDate.before(endTime)) {
        secondCheckInRotation = rotation;
      }
    }

    btnCheckInOne.setOnClickListener(new CheckInButtonListener(this, firstCheckInRotation, true));
    btnCheckInTwo.setOnClickListener(new CheckInButtonListener(this, secondCheckInRotation, false));
  }

  private Date getDateFor(final int calendarDay) {
    Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.DAY_OF_WEEK, calendarDay - calendar.get(Calendar.DAY_OF_WEEK));
    return calendar.getTime();
  }

  //this should be made better eventually...
  private void downloadVeteranSurveyOneResults() {
    if(!dialog.isShowing()){
      dialog.setMessage("Downloading Surveys");
      dialog.show();
    }
    new RoboAsyncTask<SurveyResult>(this) {
      @Override
      public SurveyResult call() throws Exception {
        if (firstCheckInRotation != null) {
          return client.getUserVeteranSurveyResults(firstCheckInRotation);
        }
        return null;
      }

      @Override
      protected void onSuccess(SurveyResult surveyResult) throws Exception {
        surveyOneResult = surveyResult;
      }

      @Override
      protected void onException(final Exception e) throws RuntimeException {
        super.onException(e);
        Log.d(getClass().getName(), e.getMessage(), e);
        Toast.makeText(getContext(), "Failed to download latest survey" + e.getMessage(), Toast.LENGTH_LONG).show();
      }

      protected void onFinally() {
        downloadVeteranSurveyTwoResults();
      }

    }.execute();
  }

  private void downloadVeteranSurveyTwoResults() {
    new RoboAsyncTask<SurveyResult>(this) {
      @Override
      public SurveyResult call() throws Exception {
        if (secondCheckInRotation != null) {
          return client.getUserVeteranSurveyResults(secondCheckInRotation);
        }
        return null;
      }

      @Override
      protected void onSuccess(SurveyResult surveyResult) throws Exception {
        surveyTwoResult = surveyResult;
      }

      @Override
      protected void onException(final Exception e) throws RuntimeException {
        super.onException(e);
        Log.d(getClass().getName(), e.getMessage(), e);
        Toast.makeText(getContext(), "Failed to download latest survey" + e.getMessage(), Toast.LENGTH_LONG).show();
      }

      protected void onFinally() {
        configureUI();
        if (dialog.isShowing()) {
          dialog.dismiss();
        }
      }
    }.execute();
  }

  private void configureUI() {
    switch (day) {
      case Calendar.SUNDAY:
        sunday.setBackgroundColor(BLUE);
        break;
      case Calendar.MONDAY:
        monday.setBackgroundColor(BLUE);
        break;
      case Calendar.TUESDAY:
        tuesday.setBackgroundColor(BLUE);
        break;
      case Calendar.WEDNESDAY:
        wednesday.setBackgroundColor(BLUE);
        break;
      case Calendar.THURSDAY:
        thursday.setBackgroundColor(BLUE);
        break;
      case Calendar.FRIDAY:
        friday.setBackgroundColor(BLUE);
        break;
      case Calendar.SATURDAY:
        saturday.setBackgroundColor(BLUE);
        break;
    }

    if(surveyOneResult != null) {
      final int color = (day == Calendar.MONDAY || day == Calendar.TUESDAY) ? YELLOW : RED;
      btnCheckInOne.setBackgroundColor(surveyOneResult.getTotalAnswered() > 0 ? GREEN : color);
    }
    if(surveyTwoResult != null) {
      final int color = (day == Calendar.FRIDAY || day == Calendar.SATURDAY) ? YELLOW : RED;
      btnCheckInTwo.setBackgroundColor(surveyTwoResult.getTotalAnswered() > 0 ? GREEN : color);
    }
  }

  private String getStartTime(final Rotation rotation) {
    String startTime = rotation.getStartTime();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    try {
      final Date parse = simpleDateFormat.parse(startTime);
      SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("MMM d ''yy");
      startTime = simpleDateFormat2.format(parse);
    } catch (java.text.ParseException e) {
      Log.d(getClass().getName(), "Failed to parse as date: " + startTime, e);
    }
    return startTime;
  }

}
