package service;

import android.content.*;
import android.net.*;

public class ConnectivityListener extends BroadcastReceiver {
  private static boolean connected = true;

  @Override
  public void onReceive(Context context, Intent intent) {
    final ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
    connected = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnected();
  }

  public static boolean isConnected() {
    return connected;
  }
}
