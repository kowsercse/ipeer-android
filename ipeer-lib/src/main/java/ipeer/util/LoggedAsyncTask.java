package ipeer.util;

import android.content.*;
import android.os.*;
import android.util.*;
import android.widget.*;
import roboguice.util.RoboAsyncTask;

import java.util.concurrent.Executor;

public abstract class LoggedAsyncTask<Result> extends RoboAsyncTask<Result> {

  protected LoggedAsyncTask(final Context context) {
    super(context);
  }

  protected LoggedAsyncTask(final Context context, final Handler handler) {
    super(context, handler);
  }

  protected LoggedAsyncTask(final Context context, final Handler handler, final Executor executor) {
    super(context, handler, executor);
  }

  protected LoggedAsyncTask(final Context context, final Executor executor) {
    super(context, executor);
  }

  @Override
  protected final void onException(final Exception ex) throws RuntimeException {
    super.onException(ex);
    Log.d(getContext().getClass().getName(), ex.getMessage(), ex);
    Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_LONG).show();
    onExceptionImpl(ex);
  }

  protected void onExceptionImpl(final Exception ex) {
    // Empty by design
  }
}
