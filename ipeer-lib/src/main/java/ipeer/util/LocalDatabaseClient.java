package ipeer.util;

import android.content.*;
import android.database.*;
import android.database.sqlite.*;
import entity.LocalVeteran;

public class LocalDatabaseClient extends SQLiteOpenHelper {
  private static final int DATABASE_VERSION = 1;
  private static final String DATABASE_NAME = "iPeer.db";

  private static final String TABLE_VETERANS = "Veterans";
  private static final String KEY_ID = "id";
  private static final String NAME = "name";
  private static final String PHONE_NUMBER = "phoneNumber";
  private static final String EMAIL = "email";

  public LocalDatabaseClient(Context context) {
    super(context, DATABASE_NAME, null, DATABASE_VERSION);
  }

  @Override
  public void onCreate(SQLiteDatabase db) {
    String sql = "CREATE TABLE " + TABLE_VETERANS + "("
        + KEY_ID + " INTEGER PRIMARY KEY,"
        + NAME + " TEXT,"
        + PHONE_NUMBER + " TEXT,"
        + EMAIL + " TEXT)";
    db.execSQL(sql);
  }

  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
  }

  public LocalVeteran getVeteran(int id) {
    final String[] columns = {KEY_ID, NAME, PHONE_NUMBER, EMAIL};
    final String[] selectionArgs = {String.valueOf(id)};
    final String selection = KEY_ID + " = ?";

    LocalVeteran localVeteran = null;
    Cursor cursor = getReadableDatabase().query(TABLE_VETERANS, columns, selection, selectionArgs, null, null, null, null);
    if (cursor != null && cursor.moveToFirst()) {
      localVeteran = getVeteran(id, cursor);
    }
    return localVeteran;
  }

  public void addVeteran(LocalVeteran veteran) {
    SQLiteDatabase db = this.getWritableDatabase();
    ContentValues values = prepareContentValues(veteran);
    db.insert(TABLE_VETERANS, null, values);
    db.close();
  }

  public void updateVeteran(LocalVeteran localVeteran) {
    if (getVeteran(localVeteran.getId()) != null) {
      ContentValues values = prepareContentValues(localVeteran);
      final String[] whereArgs = {String.valueOf(localVeteran.getId())};
      final String whereClause = KEY_ID + " = ?";
      getWritableDatabase().update(TABLE_VETERANS, values, whereClause, whereArgs);
    } else {
      addVeteran(localVeteran);
    }
  }

  private LocalVeteran getVeteran(final int id, final Cursor cursor) {
    if(cursor.getCount() == 0) {
      return null;
    }
    
    LocalVeteran localVeteran = new LocalVeteran();
    localVeteran.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
    localVeteran.setName(cursor.getString(cursor.getColumnIndex(NAME)));
    localVeteran.setPhone(cursor.getString(cursor.getColumnIndex(PHONE_NUMBER)));
    localVeteran.setEmail(cursor.getString(cursor.getColumnIndex(EMAIL)));
    return localVeteran;
  }

  private ContentValues prepareContentValues(final LocalVeteran veteran) {
    ContentValues values = new ContentValues();
    values.put(KEY_ID, veteran.getId());
    values.put(NAME, veteran.getName());
    values.put(PHONE_NUMBER, veteran.getPhone());
    values.put(EMAIL, veteran.getEmail());
    return values;
  }
}
