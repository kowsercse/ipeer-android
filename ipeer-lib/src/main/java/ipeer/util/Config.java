package ipeer.util;

import android.app.*;
import android.net.*;
import android.util.*;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class Config {
  private static Config INSTANCE = new Config();
  private final String buildVersion;
  private final String minorVersion;
  private final String majorVersion;
  private final Uri documentationUrl;
  private final String baseUrl;

  public static Config getInstance() {
    return INSTANCE;
  }

  private Config() {
    final Properties properties = loadConfigurations();

    majorVersion = properties.getProperty("ipeer.config.version.major");
    minorVersion = properties.getProperty("ipeer.config.version.minor");
    buildVersion = properties.getProperty("ipeer.config.version.build");
    baseUrl = properties.getProperty("ipeer.config.url.base");
    documentationUrl = Uri.parse(
        properties.getProperty("ipeer.config.url.documentation")
    );
  }

  public final long getUpdateInterval() {
    return AlarmManager.INTERVAL_DAY;
  }

  public final String getBaseUrl() {
    return baseUrl;
  }

  public final Uri getDocumentationUrl() {
    return documentationUrl;
  }

  public final String getMajorVersion() {
    return majorVersion;
  }

  public final String getMinorVersion() {
    return minorVersion;
  }

  public final String getBuildVersion() {
    return buildVersion;
  }

  private Properties loadConfigurations() {
    final Properties properties = new Properties();
    final List<String> configurations = Arrays.asList("config", "dev", "test", "prod");
    for (final String configuration : configurations) {
      final InputStream inputStream = this.getClass().getResourceAsStream(File.separator + configuration + ".properties");
      if(inputStream != null) {
        try {
          properties.load(inputStream);
        } catch (IOException e) {
          Log.d(getClass().getName(), "Failed to read configuration: " + configuration, e);
        }
        try {
          inputStream.close();
        } catch (IOException e) {
          Log.d(getClass().getName(), "Failed to close the stream: " + configuration, e);
        }
      }
    }

    return properties;
  }

}
