package ipeer.util;

import android.util.*;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtility {
  public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

  public static String formatDate(final Date date) {
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT);
    return simpleDateFormat.format(date);
  }

  public static Date parseDate(final String stringDate) {
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT);
    try {
      return simpleDateFormat.parse(stringDate);
    }
    catch (java.text.ParseException e) {
      Log.d(DateUtility.class.getName(), "Failed to parse submission time: " + stringDate);
      Log.d(DateUtility.class.getName(), e.getMessage());
      return null;
    }
  }

  public static Date getDateFor(final int weekDay) {
    final Calendar monday = today();
    monday.set(Calendar.DAY_OF_WEEK, weekDay);
    return monday.getTime();
  }

  private static Calendar today() {
    Calendar calendar = Calendar.getInstance();
    calendar.set(Calendar.HOUR_OF_DAY, 0); // ! clear would not reset the hour of day !
    calendar.clear(Calendar.MINUTE);
    calendar.clear(Calendar.SECOND);
    calendar.clear(Calendar.MILLISECOND);

    return calendar;
  }

}
