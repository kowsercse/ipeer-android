package ipeer.util;

import android.os.*;
import android.util.*;
import entity.*;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import org.simpleframework.xml.transform.RegistryMatcher;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.inject.Singleton;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.net.URI;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Singleton
public class Client {

  private static final URI REL_ROTATION_COLLECTION = URI.create("http://ubicomp.mscs.mu.edu/rotation");
  private static final URI REL_VETERAN = URI.create("http://ubicomp.mscs.mu.edu/veteran");

  private static final String BASE_URL = Config.getInstance().getBaseUrl();
  private static final URI ENDPOINT_URI = URI.create(BASE_URL + "/index.xml");

  private final DefaultHttpClient client;
  private final Serializer serializer;
  private Map<URI, URI> endPointLinks;
  private final DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();

  public Client() {
    DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
    RegistryMatcher registryMatcher = new RegistryMatcher();
    registryMatcher.bind(Date.class, new DateFormatTransformer(format));

    this.serializer = new Persister(registryMatcher);
    client = getUntrustedClient();
  }

  public void configure(final String username, final String password) {
    AuthScope authscope = new AuthScope(ENDPOINT_URI.getHost(), ENDPOINT_URI.getPort());
    UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(username, password);

    CredentialsProvider credentialsProvider = client.getCredentialsProvider();
    credentialsProvider.setCredentials(authscope, credentials);
  }

  public void login() throws Exception {
    HttpGet request = new HttpGet(ENDPOINT_URI);
    HttpResponse response = executeInternal(request);
    final HttpEntity entity = response.getEntity();
    endPointLinks = retrieveLinks(entity.getContent());

    entity.consumeContent();
  }

  public List<Question> getSurveyQuestions(final Survey survey) throws Exception {
    URI surveyQuestionsURI = URI.create(BASE_URL + "/survey/" + survey.getId() + "/questions");
    HttpGet request = new HttpGet(surveyQuestionsURI);
    HttpResponse response = executeInternal(request);
    InputStream inputStream = response.getEntity().getContent();
    Items items = serializer.read(Items.class, inputStream);

    List<Question> questions = items.getQuestions();
    return CollectionUtils.isEmpty(questions) ? Collections.<Question>emptyList() : questions;
  }

  public List<Veteran> getVeterans() throws Exception {
    HttpGet request = new HttpGet(endPointLinks.get(REL_VETERAN));
    HttpResponse response = executeInternal(request);
    InputStream inputStream = response.getEntity().getContent();

    Items items = serializer.read(Items.class, inputStream);
    List<Veteran> veterans = items.getVeterans();
    return CollectionUtils.isEmpty(veterans) ? Collections.<Veteran>emptyList() : veterans;
  }

  public Veteran createVeteran(final Veteran veteran) throws Exception {
    HttpPost request = new HttpPost(endPointLinks.get(REL_VETERAN));
    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    serializer.write(veteran, outputStream);
    request.setEntity(new ByteArrayEntity(outputStream.toByteArray()));
    HttpResponse response = executeInternal(request);
    String location = response.getFirstHeader("Location").getValue();
    response.getEntity().consumeContent();

    HttpGet httpGet = new HttpGet(location);
    HttpResponse veteranResponse = executeInternal(httpGet);
    InputStream inputStream = veteranResponse.getEntity().getContent();
    return serializer.read(Veteran.class, inputStream);
  }

  public void submitVeteranAnswers(final List<VeteranAnswer> veteranAnswers, Rotation rotation) throws Exception {
    final Items items = new Items();
    items.setVeteranAnswers(veteranAnswers);
    HttpPost request = new HttpPost(BASE_URL + "/veteranrotation/rotation/" + rotation.getId());
    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    serializer.write(items, outputStream);
    request.setEntity(new ByteArrayEntity(outputStream.toByteArray()));
    HttpResponse response = executeInternal(request);
    response.getEntity().consumeContent();
  }

  public List<Rotation> getRotations(final Veteran veteran) throws Exception {
    final URI uri = URI.create(BASE_URL + "/veteran/" + veteran.getId() + "/rotation/collection");
    return getRotations(uri);
  }

  public List<Rotation> getRotations() throws Exception {
    final URI uri = endPointLinks.get(REL_ROTATION_COLLECTION);
    return getRotations(uri);
  }

  private List<Rotation> getRotations(final URI uri) throws Exception {
    HttpGet request = new HttpGet(uri);
    HttpResponse response = executeInternal(request);

    InputStream inputStream = response.getEntity().getContent();
    Items items = serializer.read(Items.class, inputStream);
    final List<Rotation> rotations = items.getRotations();
    return CollectionUtils.isEmpty(rotations) ? Collections.<Rotation>emptyList() : rotations;
  }

  public SurveyResult getUserVeteranSurveyResults(final Rotation rotation) throws Exception {
    final String uri = BASE_URL + "/veteranrotation/rotation/" + rotation.getId();
    HttpGet request = new HttpGet(uri);
    HttpResponse response = executeInternal(request);

    InputStream inputStream = response.getEntity().getContent();
    return serializer.read(SurveyResult.class, inputStream);
  }

  public List<VeteranQuestionAnswer> getRotationAnswers(final Veteran veteran, final Rotation rotation) throws Exception {
    HttpGet request = new HttpGet(BASE_URL + "/veteranrotation/veteran/" + veteran.getId() + "/rotation/" + rotation.getId());
    HttpResponse response = executeInternal(request);

    InputStream inputStream = response.getEntity().getContent();
    Items items = serializer.read(Items.class, inputStream);
    List<VeteranQuestionAnswer> answers = items.getVeteranQuestionAnswer();
    return CollectionUtils.isEmpty(answers) ? Collections.<VeteranQuestionAnswer>emptyList() : answers;
  }

  public Mentor getMentor() throws Exception {
    HttpGet request = new HttpGet(BASE_URL + "/profile/mentor");
    HttpResponse response = executeInternal(request);

    InputStream inputStream = response.getEntity().getContent();
    return serializer.read(Mentor.class, inputStream);
  }

  public List<SurveyResult> getSurveyResults(Veteran veteran) throws Exception {
    HttpGet request = new HttpGet(BASE_URL + "/veteranrotation/veteran/" + veteran.getId());
    HttpResponse response = executeInternal(request);

    InputStream inputStream = response.getEntity().getContent();
    return serializer.read(Items.class, inputStream).getSurveyResult();
  }

  private URI getResetPasswordToken(final Veteran veteran) throws Exception {
    HttpGet request = new HttpGet(BASE_URL + "/veteran/" + veteran.getId() + "/resettoken");
    HttpResponse response = executeInternal(request);

    InputStream content = null;
    try {
      content = response.getEntity().getContent();
      Scanner scanner = new Scanner(content);
      final String urlString = scanner.nextLine();
      response.getEntity().consumeContent();
      return URI.create(urlString);
    }
    finally {
      if(content != null) {
        content.close();
      }
    }
  }

  public void resetPassword(final Veteran veteran) throws Exception {
    HttpGet request = new HttpGet(BASE_URL + "/veteran/" + veteran.getId() + "/resettoken");
    HttpResponse response = executeInternal(request);
    response.getEntity().consumeContent();
  }

  public URI checkForUpdate() throws IOException {
    final String latestVersionUrl = BASE_URL + "/version/latest?current=" + Config.getInstance().getBuildVersion();
    HttpGet request = new HttpGet(latestVersionUrl);
    HttpResponse response = executeInternal(request);

    InputStream content = null;
    try {
      content = response.getEntity().getContent();
      Scanner scanner = new Scanner(content);

      String versionUrl = null;
      while (scanner.hasNext()) {
        versionUrl = scanner.nextLine();
      }
      response.getEntity().consumeContent();

      return versionUrl == null ? null : URI.create(versionUrl + "/");
    }
    finally {
      if (content != null) {
        content.close();
      }
    }
  }

  public File downloadFile(final URI downloadUri, final String apk) throws IOException {
    final URI apkDownloadUri = downloadUri.resolve(apk);
    HttpGet request = new HttpGet(apkDownloadUri);
    HttpResponse response = executeInternal(request);
    final Header[] headers = response.getHeaders("Content-Length");

    File output = getOutputFile(downloadUri, response, apk);
    final boolean sameFileExists = sameFileExists(output, headers);
    if (sameFileExists) {
      response.getEntity().consumeContent();
    } else {
      output.delete();
      writeDownloadedFile(output, response);
    }

    return output;
  }

  private boolean sameFileExists(final File output, final Header[] headers) {
    return output.exists() && headers.length == 1 && Long.valueOf(headers[0].getValue()) == output.length();
  }

  private void writeDownloadedFile(final File output, final HttpResponse response) throws IOException {
    InputStream stream = null;
    FileOutputStream fileOutputStream = null;
    try {
      stream = response.getEntity().getContent();
      fileOutputStream = new FileOutputStream(output.getPath());
      byte data[] = new byte[4096];
      int count;
      while ((count = stream.read(data)) != -1) {
        fileOutputStream.write(data, 0, count);
        fileOutputStream.flush();
      }
    }
    finally {
      if (stream != null) {
        stream.close();
      }
      if (fileOutputStream != null) {
        fileOutputStream.close();
      }
    }
  }

  private File getOutputFile(final URI downloadUri, final HttpResponse response, final String apk) {
    final Header[] headers = response.getHeaders("Content-Disposition");
    String fileName = null;
    if (headers != null && headers.length == 1) {
      final String raw = headers[0].getValue();
      if (raw != null && raw.contains("=")) {
        fileName = raw.split("=")[1].replace("\"", "");
      }
    }

    if (fileName == null) {
      String[] segments = downloadUri.getPath().split("/");
      String idString = segments[segments.length - 1];
      fileName = String.format("%s-%d.%s", apk, Integer.parseInt(idString), "apk");
    }

    return new File(Environment.getExternalStorageDirectory(), fileName);
  }

  private DefaultHttpClient getUntrustedClient() {
    HttpParams httpParams = new BasicHttpParams();
    SchemeRegistry schemeRegistry = new SchemeRegistry();
    schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
    schemeRegistry.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));
    SingleClientConnManager mgr = new SingleClientConnManager(httpParams, schemeRegistry);

    return new DefaultHttpClient(mgr, httpParams);
  }

  private HttpResponse executeInternal(final HttpUriRequest httpUriRequest) throws IOException {
    Log.d(getClass().getName(), "Sending request: " + httpUriRequest.getMethod() + " " + httpUriRequest.getURI().toString());
    HttpResponse response = client.execute(httpUriRequest);
    int statusCode = response.getStatusLine().getStatusCode();
    if (statusCode == 401) {
      response.getEntity().consumeContent();
      throw new RuntimeException("Wrong username or password");
    }
    else if (statusCode >= 400 && statusCode < 500) {
      response.getEntity().consumeContent();
      throw new RuntimeException("Permission Denied: " + statusCode);
    }
    else if (statusCode >= 500) {
      response.getEntity().consumeContent();
      Log.d(getClass().getName(), "Status code[" + statusCode + "] " + httpUriRequest.getURI().toString());
      throw new RuntimeException("Server is down: " + statusCode);
    }
    Log.d(getClass().getName(), "Completed: " + httpUriRequest.getMethod() + " " + httpUriRequest.getURI().toString());

    return response;
  }

  private void logError(final HttpResponse response, final boolean enabled) throws IOException {
    if (!enabled) {
      return;
    }
    BufferedReader r = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
    StringBuilder total = new StringBuilder();
    String line;
    while ((line = r.readLine()) != null) {
      total.append(line);
    }
    Log.d(getClass().getName(), total.toString());
  }

  private Map<URI, URI> retrieveLinks(final InputStream content) throws Exception {
    Map<URI, URI> links = new HashMap<URI, URI>();
    DocumentBuilder dBuilder = documentBuilderFactory.newDocumentBuilder();
    Document document = dBuilder.parse(content);
    document.getDocumentElement().normalize();

    NodeList nodeList = document.getElementsByTagName("link");
    for (int i = 0; i < nodeList.getLength(); i++) {
      final Node node = nodeList.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        final Element element = (Element) node;
        links.put(
            URI.create(element.getAttribute("rel")),
            URI.create(element.getAttribute("href"))
        );
      }
    }

    return links;
  }

}
