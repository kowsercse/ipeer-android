package ipeer.util;

import android.app.*;
import android.content.*;
import android.telephony.*;
import android.widget.*;

import java.util.ArrayList;

public class AndroidUtils {

  public static void sendSMS(String phoneNumber, String message, Context context) {
    ArrayList<PendingIntent> sentPendingIntents = new ArrayList<PendingIntent>();
    ArrayList<PendingIntent> deliveredPendingIntents = new ArrayList<PendingIntent>();
    PendingIntent sentPI = PendingIntent.getBroadcast(context, 0, new Intent(context, SmsSentReceiver.class), 0);
    PendingIntent deliveredPI = PendingIntent.getBroadcast(context, 0, new Intent(context, SmsDeliveredReceiver.class), 0);
    try {
      SmsManager sms = SmsManager.getDefault();
      ArrayList<String> mSMSMessage = sms.divideMessage(message);
      for (int i = 0; i < mSMSMessage.size(); i++) {
        sentPendingIntents.add(i, sentPI);
        deliveredPendingIntents.add(i, deliveredPI);
      }
      sms.sendMultipartTextMessage(phoneNumber, null, mSMSMessage, sentPendingIntents, deliveredPendingIntents);
    } catch (Exception e) {
      e.printStackTrace();
      Toast.makeText(context, "SMS sending failed...", Toast.LENGTH_SHORT).show();
    }
  }

  public class SmsDeliveredReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent arg1) {
      switch (getResultCode()) {
        case Activity.RESULT_OK:
          Toast.makeText(context, "SMS delivered", Toast.LENGTH_SHORT).show();
          break;
        case Activity.RESULT_CANCELED:
          Toast.makeText(context, "SMS not delivered", Toast.LENGTH_SHORT).show();
          break;
      }
    }
  }

  public class SmsSentReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent arg1) {
      switch (getResultCode()) {
        case Activity.RESULT_OK:
          Toast.makeText(context, "SMS Sent", Toast.LENGTH_SHORT).show();
          break;
        case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
          Toast.makeText(context, "SMS generic failure", Toast.LENGTH_SHORT).show();
          break;
        case SmsManager.RESULT_ERROR_NO_SERVICE:
          Toast.makeText(context, "SMS no service", Toast.LENGTH_SHORT).show();
          break;
        case SmsManager.RESULT_ERROR_NULL_PDU:
          Toast.makeText(context, "SMS null PDU", Toast.LENGTH_SHORT).show();
          break;
        case SmsManager.RESULT_ERROR_RADIO_OFF:
          Toast.makeText(context, "SMS radio off", Toast.LENGTH_SHORT).show();
          break;
      }
    }
  }
}
