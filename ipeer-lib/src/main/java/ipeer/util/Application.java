package ipeer.util;

import entity.Mentor;
import entity.Veteran;

import javax.inject.Singleton;
import java.util.List;

@Singleton
public class Application {

  private List<Veteran> veterans;
  private Mentor mentor;
  private Veteran veteran;

  public void setVeterans(final List<Veteran> veterans) {
    this.veterans = veterans;
  }

  public List<Veteran> getVeterans() {
    return veterans;
  }

  public Mentor getMentor() {
    return mentor;
  }

  public void setMentor(final Mentor mentor) {
    this.mentor = mentor;
  }

  public Veteran getVeteran() {
    return veteran;
  }

  public void setVeteran(final Veteran veteran) {
    this.veteran = veteran;
  }
}
