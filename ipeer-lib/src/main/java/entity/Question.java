package entity;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;

import java.util.List;

public class Question {

  @Element
  private int id;
  @Element
  private String title;
  @Element
  private int survey;
  @ElementList(entry = "answer", inline = true, required = false)
  private List<Answer> answers;

  public int getId() {
    return id;
  }

  public void setId(final int id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(final String title) {
    this.title = title;
  }

  public int getSurvey() {
    return survey;
  }

  public void setSurvey(final int survey) {
    this.survey = survey;
  }

  public List<Answer> getAnswers() {
    return answers;
  }

  public void setAnswers(final List<Answer> answers) {
    this.answers = answers;
  }

  @Override
  public String toString() {
    return title;
  }
}
