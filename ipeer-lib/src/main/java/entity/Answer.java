package entity;

public class Answer {
  private Integer id;
  private String label;
  private Integer value;
  private Integer position;
  private Integer answerSetId;

  public Integer getId() {
    return id;
  }

  public void setId(final Integer id) {
    this.id = id;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(final String label) {
    this.label = label;
  }

  public Integer getValue() {
    return value;
  }

  public void setValue(final Integer value) {
    this.value = value;
  }

  public Integer getPosition() {
    return position;
  }

  public void setPosition(final Integer position) {
    this.position = position;
  }

  public Integer getAnswerSetId() {
    return answerSetId;
  }

  public void setAnswerSetId(final Integer answerSetId) {
    this.answerSetId = answerSetId;
  }
}
