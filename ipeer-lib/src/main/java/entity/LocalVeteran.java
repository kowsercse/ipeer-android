package entity;


public class LocalVeteran {
  private int id;
  private String name;
  private String phone;
  private String email;

  public LocalVeteran() {
  }

  public LocalVeteran(final int id) {
    this.id = id;
    this.name = "";
    this.email = "";
    this.phone = "";
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name == null ? "" : name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getPhone() {
    return phone == null ? "" : phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getEmail() {
    return email == null ? "" : email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  @Override
  public String toString() {
    return "Name: " + this.getName() + "\n" +
        "Phone: " + this.getPhone() + "\n" +
        "Email: " + this.getEmail();
  }
}
