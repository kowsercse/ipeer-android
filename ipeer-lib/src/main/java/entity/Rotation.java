package entity;

public class Rotation {

  private Integer id;
  private String startTime;
  private String endTime;
  private boolean active;
  private boolean enabled;
  private Survey survey;

  public Integer getId() {
    return id;
  }

  public void setId(final Integer id) {
    this.id = id;
  }

  public String getStartTime() {
    return startTime;
  }

  public void setStartTime(final String startTime) {
    this.startTime = startTime;
  }

  public String getEndTime() {
    return endTime;
  }

  public void setEndTime(final String endTime) {
    this.endTime = endTime;
  }

  public boolean isActive() {
    return active;
  }

  public void setActive(final boolean active) {
    this.active = active;
  }

  public boolean isEnabled() {
    return enabled;
  }

  public void setEnabled(final boolean enabled) {
    this.enabled = enabled;
  }

  public Survey getSurvey() {
    return survey;
  }

  public void setSurvey(final Survey survey) {
    this.survey = survey;
  }

  @Override
  public String toString() {
    return "Rotation{" +
        "startTime='" + startTime + '\'' +
        ", survey=" + survey +
        '}';
  }
}
