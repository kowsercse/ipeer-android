package entity;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

@Root(name = "items")
public class Items {

  @ElementList(entry = "veteran", inline = true, required = false)
  private List<Veteran> veterans;

  @ElementList(entry = "survey", inline = true, required = false)
  private List<Survey> surveys;

  @ElementList(entry = "question", inline = true, required = false)
  private List<Question> questions;

  @ElementList(entry = "surveyResult", inline = true, required = false)
  private List<SurveyResult> surveyResult;

  @ElementList(entry = "veteranQuestionAnswer", inline = true, required = false)
  private List<VeteranQuestionAnswer> veteranQuestionAnswer;

  @ElementList(entry = "veteranAnswer", inline = true, required = false)
  private List<VeteranAnswer> veteranAnswers;

  @ElementList(entry = "rotation", inline = true, required = false)
  private List<Rotation> rotations;

  public List<Veteran> getVeterans() {
    return veterans;
  }

  public void setVeterans(final List<Veteran> veterans) {
    this.veterans = veterans;
  }

  public List<Survey> getSurveys() {
    return surveys;
  }

  public void setSurveys(final List<Survey> surveys) {
    this.surveys = surveys;
  }

  public List<Question> getQuestions() {
    return questions;
  }

  public void setQuestions(final List<Question> questions) {
    this.questions = questions;
  }

  public List<SurveyResult> getSurveyResult() {
    return surveyResult;
  }

  public void setSurveyResult(final List<SurveyResult> surveyResult) {
    this.surveyResult = surveyResult;
  }

  public List<VeteranQuestionAnswer> getVeteranQuestionAnswer() {
    return veteranQuestionAnswer;
  }

  public void setVeteranQuestionAnswer(final List<VeteranQuestionAnswer> veteranQuestionAnswer) {
    this.veteranQuestionAnswer = veteranQuestionAnswer;
  }

  public List<VeteranAnswer> getVeteranAnswers() {
    return veteranAnswers;
  }

  public void setVeteranAnswers(final List<VeteranAnswer> veteranAnswers) {
    this.veteranAnswers = veteranAnswers;
  }

  public List<Rotation> getRotations() {
    return rotations;
  }

  public void setRotations(final List<Rotation> rotations) {
    this.rotations = rotations;
  }
}
