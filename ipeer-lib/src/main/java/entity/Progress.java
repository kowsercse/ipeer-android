package entity;

public class Progress {
  private int score;
  private int result;

  public int getScore() {
    return score;
  }

  public void setScore(final int score) {
    this.score = score;
  }

  public int getResult() {
    return result;
  }

  public void setResult(final int result) {
    this.result = result;
  }
}
