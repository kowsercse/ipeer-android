package entity;

import java.util.Date;

public class SurveyResult {
  private Integer surveyId;
  private Integer rotationId;
  private Float averageScore;
  private Integer totalAnswered;
  private Boolean improved;
  private Date startTime;

  public Integer getRotationId() {
    return rotationId;
  }

  public void setRotationId(final Integer rotationId) {
    this.rotationId = rotationId;
  }

  public Integer getSurveyId() {
    return surveyId;
  }

  public void setSurveyId(final Integer surveyId) {
    this.surveyId = surveyId;
  }

  public Float getAverageScore() {
    return averageScore;
  }

  public void setAverageScore(final Float averageScore) {
    this.averageScore = averageScore;
  }

  public Integer getTotalAnswered() {
    return totalAnswered;
  }

  public void setTotalAnswered(final Integer totalAnswered) {
    this.totalAnswered = totalAnswered;
  }

  public Boolean isImproved() {
    return improved;
  }

  public void setImproved(final Boolean improved) {
    this.improved = improved;
  }

  public Date getStartTime() {
    return startTime;
  }

  public void setStartTime(final Date startTime) {
    this.startTime = startTime;
  }
}
