package entity;

import java.sql.Date;

public class VeteranAnswer {
  private int id;
  private int question;
  private int answer;
  private int veteran;
  private Date submitDateTime;

  public int getId() {
    return id;
  }

  public void setId(final int id) {
    this.id = id;
  }

  public int getQuestion() {
    return question;
  }

  public void setQuestion(final int question) {
    this.question = question;
  }

  public int getAnswer() {
    return answer;
  }

  public void setAnswer(final int answer) {
    this.answer = answer;
  }

  public int getVeteran() {
    return veteran;
  }

  public void setVeteran(final int veteran) {
    this.veteran = veteran;
  }

  public Date getSubmitDateTime() {
    return submitDateTime;
  }


  public void setSubmitDateTime(final Date submitDateTime) {
    this.submitDateTime = submitDateTime;
  }

  @Override
  public String toString() {
    return "Question ID: " + question + ", Veteran ID: " + veteran + ", Answer: " + answer;
  }


}
