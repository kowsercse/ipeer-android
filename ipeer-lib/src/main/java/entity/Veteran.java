package entity;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;

import java.util.List;

public class Veteran {
  @Element(required = false)
  private Integer id;

  @Element(required = false)
  private String startDate;

  @Element(required = false)
  private Integer user;

  @Element(required = false)
  private Integer mentor;

  @ElementList(entry = "surveyResult", inline = true, required = false)
  private List<SurveyResult> surveyResults;

  public Veteran() {
  }

  public Veteran(final int id) {
    this.id = id;
  }

  public int getId() {
    return id;
  }

  public void setId(final int id) {
    this.id = id;
  }

  public String getStartDate() {
    return startDate;
  }

  public void setStartDate(final String startDate) {
    this.startDate = startDate;
  }

  public int getUser() {
    return user;
  }

  public void setUser(final int user) {
    this.user = user;
  }

  public int getMentor() {
    return mentor;
  }

  public void setMentor(final int mentor) {
    this.mentor = mentor;
  }

  public List<SurveyResult> getSurveyResults() {
    return surveyResults;
  }

  public void setSurveyResults(final List<SurveyResult> surveyResults) {
    this.surveyResults = surveyResults;
  }

  @Override
  public String toString() {
    return "Participant id: " + id;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) return true;
    if (!(o instanceof Veteran)) return false;

    final Veteran veteran = (Veteran) o;

    if (id != veteran.id) return false;

    return true;
  }

  @Override
  public int hashCode() {
    return id;
  }
}
