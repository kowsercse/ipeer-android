package entity;

public class VeteranQuestionAnswer {
  private int question;
  private String answer;
  private String title;

  public int getQuestion() {
    return question;
  }

  public void setQuestion(final int question) {
    this.question = question;
  }

  public String getAnswer() {
    return answer;
  }

  public void setAnswer(final String answer) {
    this.answer = answer;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(final String title) {
    this.title = title;
  }
}
